package com.infectedbytes.solarcolony;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by Henrik on 13.01.2016.
 */
public class Asteroid implements Json.Serializable {
    private TextureRegion textureRegion;
    public Vector2 position = new Vector2();
    public Vector2 direction = new Vector2();
    private Vector2 tmp = new Vector2();
    private float angle;
    private float spin;
    public int probability;

    private int originX, originY;
    public int width, height;

    public Asteroid() {}
    public Asteroid(Asteroid other) {
        this.textureRegion = other.textureRegion;
        this.position.set(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() * Util.nextFloat());
        this.direction.set((Util.nextFloat() - 2) * 20, (Util.nextFloat() - 0.5f) * 5);
        this.angle = Util.nextFloat();
        this.spin = (Util.nextFloat() - 0.5f) * 40;
        width = textureRegion.getRegionWidth();
        height = textureRegion.getRegionHeight();
        originX = width / 2;
        originY = height / 2;
    }

    public void update(float delta) {
        this.angle += spin * delta;
        this.tmp.set(direction).scl(delta);
        this.position.add(tmp);
    }

    public void render(SpriteBatch batch) {
        batch.draw(textureRegion, position.x, position.y, originX, originY, width, height, 1, 1, angle);
    }

    @Override
    public void write(Json json) {
        Util.writeTexureRegion(json, "textureRegion", textureRegion);
        json.writeValue("position", this.position);
        json.writeValue("direction", this.direction);
        json.writeValue("angle", angle);
        json.writeValue("spin", spin);
        json.writeValue("probability", probability);
    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        JsonValue region = jsonData.get("textureRegion");
        textureRegion = new TextureRegion(Resources.asteroids, region.getInt("x"), region.getInt("y"), region.getInt("width"), region.getInt("height"));
        position = json.readValue("position", Vector2.class, jsonData);
        direction = json.readValue("direction", Vector2.class, jsonData);
        angle = jsonData.getFloat("angle");
        spin = jsonData.getFloat("spin");
        probability = jsonData.getInt("probability");
        width = textureRegion.getRegionWidth();
        height = textureRegion.getRegionHeight();
        originX = width / 2;
        originY = height / 2;
    }
}
