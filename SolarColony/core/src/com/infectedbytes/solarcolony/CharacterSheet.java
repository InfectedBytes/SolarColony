package com.infectedbytes.solarcolony;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.infectedbytes.solarcolony.enums.Direction;

/**
 * Created by Henrik on 26.12.2015.
 */
public class CharacterSheet {
    private TextureRegion character;
    private TextureRegion[][] frames; // rows x columns

    public CharacterSheet(TextureRegion character) {
        this.character = character;
        this.frames = new TextureRegion[4][4];
        for(int y = 0; y < 4; y++) { // 0-3 matches Direction.NORTH, etc.
            for(int x = 1; x < 4; x++) {
                this.frames[y][x] = new TextureRegion(character,
                        (x - 1) * Util.CHARACTER_WIDTH, y * Util.CHARACTER_HEIGHT,
                        Util.CHARACTER_WIDTH, Util.CHARACTER_HEIGHT);
            }
            this.frames[y][0] = this.frames[y][2];
        }
    }

    public TextureRegion getFrame(Direction heading, int frame) {
        frame = frame % 4;
        return frames[heading.ordinal()][frame];
    }
}
