package com.infectedbytes.solarcolony;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.infectedbytes.solarcolony.ui.ControlMenu;
import com.infectedbytes.solarcolony.ui.GlobalResources;
import com.infectedbytes.solarcolony.ui.InfoPanel;
import com.infectedbytes.solarcolony.ui.StatusBar;
import com.infectedbytes.solarcolony.useractions.InfoAction;
import com.infectedbytes.solarcolony.useractions.UserAction;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Henrik on 22.12.2015.
 */
public class GameState implements ScreenState {
    private Viewport viewport;
    private OrthographicCamera camera;
    private GameWorld world;
    private Stage stage;
    private Main main;
    private InputMultiplexer multiplexer;
    public ShapeRenderer shapeRenderer;
    private GlobalResources globalResources;
    private final FileHandle savegames = Gdx.files.local("savegames");
    private String savegame;
    public boolean godmode; // aka debug mode
    private StatusBar statusBar;

    public boolean isPaused() { return statusBar.isPaused(); }

    public void toggleGodmode() { godmode = !godmode; }

    public void toggleResourceScreen() { globalResources.setVisible(!globalResources.isVisible()); }

    public UserAction getUserAction() { return userAction; }

    public void setUserAction(UserAction userAction) { this.userAction = userAction; }

    private UserAction userAction;

    public OrthographicCamera getCamera() { return camera; }

    public GameWorld getWorld() { return world; }

    public void exit() { main.pop(); }

    public void popup(String text) {
        Dialog dialog = new Dialog("Info", Resources.skin);
        dialog.text(text);
        dialog.button("OK");
        dialog.show(stage);
    }

    /**
     * creates a new game by loading the given savegame.
     *
     * @param main
     * @param savegame the savegame name, it will be loaded from the savegames directory.
     */
    @SuppressWarnings("unchecked")
    public GameState(Main main, String savegame) {
        this.main = main;
        this.savegame = savegame;
        shapeRenderer = new ShapeRenderer();
        stage = new Stage(new ScreenViewport());
        camera = new OrthographicCamera();
        viewport = new ExtendViewport(800, 480, camera);
        viewport.apply();

        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(new Input());

        TooltipManager.getInstance().initialTime = 0;
        TooltipManager.getInstance().instant();

        stage.addActor(new ControlMenu(this));
        stage.addActor(new InfoPanel(this));
        stage.addActor(statusBar = new StatusBar(this));

        globalResources = new GlobalResources(this);
        stage.addActor(globalResources);
        globalResources.setVisible(false);
        load();
    }

    public void load() {
        userAction = null;
        world = Util.json.fromJson(GameWorld.class, savegames.child(savegame));
        world.gameState = this;
    }

    public void save() {
        try {
            OutputStream fw = savegames.child(savegame).write(false);
            fw.write(Util.json.prettyPrint(world).getBytes());
            fw.close();
        } catch(IOException e) {
            throw new GdxRuntimeException(e);
        }
    }

    @Override
    public void update(float delta) {
        world.update(delta);
        stage.act(delta);
        if(userAction == null) userAction = new InfoAction(this);
        userAction.update(delta);
    }

    @Override
    public void render(SpriteBatch batch) {
        camera.update();
        shapeRenderer.setProjectionMatrix(camera.combined);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        world.render(batch);
        if(userAction != null)
            userAction.render(batch);
        batch.end();
        stage.draw();
    }

    @Override
    public void dispose() { stage.dispose(); }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
        stage.getViewport().update(width, height, true);
    }

    @Override
    public InputProcessor inputProcessor() { return multiplexer; }

    private class Input extends InputAdapter {
        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            if(userAction != null) {
                Vector3 pos = new Vector3(screenX, screenY, 0);
                camera.unproject(pos);
                userAction.mouseMoved((int)pos.x, (int)pos.y);
            }
            return true;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            if(userAction != null) {
                Vector3 pos = new Vector3(screenX, screenY, 0);
                camera.unproject(pos);
                userAction.mouseDragged((int)pos.x, (int)pos.y);
            }
            return true;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            if(userAction != null) {
                Vector3 pos = new Vector3(screenX, screenY, 0);
                camera.unproject(pos);
                userAction.mouseDown((int)pos.x, (int)pos.y, button);
            }
            return true;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if(userAction != null) {
                Vector3 pos = new Vector3(screenX, screenY, 0);
                camera.unproject(pos);
                userAction.mouseUp((int)pos.x, (int)pos.y, button);
            }
            return true;
        }
    }
}
