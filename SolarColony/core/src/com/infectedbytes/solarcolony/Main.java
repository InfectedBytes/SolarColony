package com.infectedbytes.solarcolony;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

public class Main extends ApplicationAdapter {
    public static boolean godmodeUnlocked;
    private SpriteBatch batch;
    private Stack<ScreenState> screens = new Stack<ScreenState>();

    public void push(ScreenState screen) {
        screens.push(screen);
        revalidate();
    }

    public ScreenState pop() {
        ScreenState screen = screens.pop();
        screen.dispose();
        revalidate();
        return screen;
    }

    public ScreenState peek() { return screens.peek(); }

    private void revalidate() {
        ScreenState screen = peek();
        if(screen != null) screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void create() {
        Resources.load();
        batch = new SpriteBatch();
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(new InputAdapter() {
            private boolean state;

            @Override
            public boolean keyDown(int keycode) {
                if(state && keycode == Input.Keys.NUM_2) {
                    godmodeUnlocked = true;
                    return true;
                }
                state = false;
                if(keycode == Input.Keys.NUM_4) {
                    state = true;
                    return true;
                }
                if(keycode == Input.Keys.F10) {
                    ScreenshotFactory.saveScreenshot();
                    return true;
                }
                return false;
            }
        });
        multiplexer.addProcessor(new InputRedirect());
        Gdx.input.setInputProcessor(multiplexer);
        push(new StartScreen(this));
    }

    @Override
    public void resize(int width, int height) {
        if(!screens.isEmpty())
            peek().resize(width, height);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if(!screens.isEmpty()) {
            peek().update(Gdx.graphics.getDeltaTime());
            peek().render(batch);
        }
    }

    private class InputRedirect implements InputProcessor {
        @Override
        public boolean keyDown(int keycode) {
            if(peek() != null) return peek().inputProcessor().keyDown(keycode);
            return false;
        }

        @Override
        public boolean keyUp(int keycode) {
            if(peek() != null) return peek().inputProcessor().keyUp(keycode);
            return false;
        }

        @Override
        public boolean keyTyped(char character) {
            if(peek() != null) return peek().inputProcessor().keyTyped(character);
            return false;
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            if(peek() != null) return peek().inputProcessor().touchDown(screenX, screenY, pointer, button);
            return false;
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            if(peek() != null) return peek().inputProcessor().touchUp(screenX, screenY, pointer, button);
            return false;
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            if(peek() != null) return peek().inputProcessor().touchDragged(screenX, screenY, pointer);
            return false;
        }

        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            if(peek() != null) return peek().inputProcessor().mouseMoved(screenX, screenY);
            return false;
        }

        @Override
        public boolean scrolled(int amount) {
            if(peek() != null) return peek().inputProcessor().scrolled(amount);
            return false;
        }
    }
}
