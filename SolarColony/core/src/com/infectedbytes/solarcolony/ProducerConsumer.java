package com.infectedbytes.solarcolony;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.msg.Telegraph;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.infectedbytes.solarcolony.enums.Message;
import com.infectedbytes.solarcolony.enums.Resource;
import com.infectedbytes.solarcolony.enums.StorageType;
import com.infectedbytes.solarcolony.statemachines.MoveJob;

/**
 * Created by Henrik on 02.01.2016.
 */
public class ProducerConsumer implements Telegraph, Json.Serializable {
    public Array<Quantity> consume = new Array<Quantity>();
    public Array<Quantity> produce = new Array<Quantity>();
    public Array<MoveJob> scheduledJobs = new Array<MoveJob>();

    private float tick;
    private float time;
    private int multiplier = 1;
    public boolean offline;
    private boolean couldWork;

    public boolean isWaiting() { return !couldWork || scheduledJobs.size > 0; }

    public int getMultiplier() { return multiplier; }

    public void setMultiplier(int multiplier) { this.multiplier = multiplier; }

    /**
     * This constructor should only be used by json deserialization
     */
    @Deprecated
    @SuppressWarnings("unused")
    public ProducerConsumer() {}

    public ProducerConsumer(ProducerConsumer pc) {
        this.tick = pc.tick;
        this.time = pc.time;
        this.multiplier = pc.multiplier;
        this.offline = pc.offline;
        for(Quantity q : pc.consume) consume.add(new Quantity(q));
        for(Quantity q : pc.produce) produce.add(new Quantity(q));
    }

    public void update(float delta, GameWorld world, GameEntity owner) {
        if(offline) return;
        time += delta;
        if(time > tick) {
            time = 0;
            execute(world, owner);
        }
    }

    private void execute(GameWorld world, GameEntity owner) {
        couldWork = false;
        for(int i = 0; i < multiplier; i++) {
            if(canProduce(world, owner.storages)) {
                scheduledJobs.clear();
                consumeAll(world, owner.storages);
                produceAll(world, owner.storages);
                couldWork = true;
            } else {
                getInput(world, owner);
                getOutput(world, owner);
            }
        }
    }

    private void consumeAll(GameWorld world, Array<Storage> storages) {
        Quantity tmp = new Quantity(Resource.Energy);
        for(Quantity q : consume) {
            tmp.resource = q.resource;
            tmp.amount = 0;
            if(q.resource.globalAccess) {
                world.getGlobalResource(tmp, q.amount);
            } else {
                int amount = q.amount;
                for(Storage storage : storages) {
                    if(storage.getType() == StorageType.Input || storage.getType() == StorageType.Global)
                        amount -= storage.get(tmp, amount);
                }
            }
        }
    }

    private void produceAll(GameWorld world, Array<Storage> storages) {
        for(Quantity q : produce) {
            Quantity tmp = new Quantity(q);
            if(q.resource.globalAccess) {
                world.addGlobalResource(tmp);
            } else {
                for(Storage storage : storages) {
                    if(storage.getType() == StorageType.Output || storage.getType() == StorageType.Global)
                        storage.add(tmp);
                }
            }
        }
    }

    public Resource checkConsume(GameWorld world, Array<Storage> storages) {
        for(Quantity q : consume) {
            if(q.resource.globalAccess) {
                if(world.getGlobalAmount(q.resource) < q.amount) return q.resource;
            } else {
                if(!hasEnoughItems(storages, q)) return q.resource;
            }
        }
        return null;
    }

    public Resource checkProduce(GameWorld world, Array<Storage> storages) {
        // check free space for result
        for(Quantity q : produce) {
            if(q.resource.globalAccess) {
                if(world.getFreeSpace(q.resource) < q.amount) return q.resource;
            } else {
                if(!hasEnoughSpace(storages, q)) return q.resource;
            }
        }
        return null;
    }

    public boolean canProduce(GameWorld world, Array<Storage> storages) {
        if(checkConsume(world, storages) != null) return false;
        if(checkProduce(world, storages) != null) return false;
        return true;
    }

    private boolean hasEnoughItems(Array<Storage> storages, Quantity quantity) {
        int sum = 0;
        for(Storage storage : storages) {
            if(storage.getType() == StorageType.Input || storage.getType() == StorageType.Global)
                sum += storage.getAmount(quantity.resource);
        }
        return sum >= quantity.amount;
    }

    private boolean hasEnoughSpace(Array<Storage> storages, Quantity quantity) {
        int free = 0;
        for(Storage storage : storages) {
            if((storage.getType() == StorageType.Output || storage.getType() == StorageType.Global) && storage.isAllowed(quantity.resource))
                free += storage.freeSpace();
        }
        return free >= quantity.amount;
    }

    private void getInput(GameWorld world, GameEntity owner) {
        for(Quantity q : consume) {
            if(!q.resource.globalAccess) {
                if(isResourceScheduled(q.resource)) continue;
                if(world.getGlobalAmount(q.resource) < q.amount) continue;
                MoveJob job = new MoveJob(q.resource, q.amount, null, owner);
                job.optionalMultiplier = 5;
                Messenger.manager.dispatchMessage(owner, null, Message.MoveResources.ordinal(), job, true);
            }
        }
    }

    private void getOutput(GameWorld world, GameEntity owner) {
        for(Quantity q : produce) {
            if(!q.resource.globalAccess) {
                if(isResourceScheduled(q.resource)) continue;
                if(hasEnoughSpace(owner.storages, q)) continue;
                int sum = 0;
                for(Storage storage : owner.storages) sum += storage.getAmount(q.resource);
                MoveJob job = new MoveJob(q.resource, sum, (Room)owner, null);
                Messenger.manager.dispatchMessage(owner, null, Message.MoveResources.ordinal(), job, true);
            }
        }
    }

    @Override
    public boolean handleMessage(Telegram msg) {
        if(msg.message == Message.MoveResources.ordinal()) {
            MoveJob job = (MoveJob)msg.extraInfo;
            if(job.accepted) scheduledJobs.add(job);
        }
        return false;
    }

    public boolean isResourceScheduled(Resource resource) {
        for(MoveJob job : scheduledJobs) if(job.getResource() == resource) return true;
        return false;
    }

    @Override
    public void write(Json json) {
        json.writeValue("consume", consume, Quantity.class);
        json.writeValue("produce", produce, Quantity.class);
        //json.writeValue("scheduledJobs", scheduledJobs, MoveJob.class);
        //jobs are serialized by the citizen
        json.writeValue("tick", tick);
        json.writeValue("time", time);
        json.writeValue("multiplier", multiplier);
        json.writeValue("offline", offline);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void read(Json json, JsonValue jsonData) {
        consume = json.readValue("consume", Array.class, Quantity.class, jsonData);
        produce = json.readValue("produce", Array.class, Quantity.class, jsonData);
        //scheduledJobs = json.readValue("scheduledJobs", Array.class, MoveJob.class, jsonData);
        //jobs are deserialized indirectly
        tick = jsonData.getFloat("tick");
        time = jsonData.getFloat("time");
        if(jsonData.has("multiplier")) multiplier = jsonData.getInt("multiplier");
        if(jsonData.has("offline")) offline = jsonData.getBoolean("offline");
    }
}
