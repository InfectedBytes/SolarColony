package com.infectedbytes.solarcolony;

import com.infectedbytes.solarcolony.enums.Resource;

/**
 * Created by Henrik on 02.01.2016.
 */
public class Quantity {
    public Resource resource;
    public int amount;

    /**
     * This constructor should only be used by json deserialization
     */
    @Deprecated
    @SuppressWarnings("unused")
    public Quantity() {}
    public Quantity(Resource resource) {
        this(resource, 0);
    }

    public Quantity(Quantity other) {
        this.resource = other.resource;
        this.amount = other.amount;
    }

    public Quantity(Resource resource, int amount) {
        this.resource = resource;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return resource.toString() + ":" + amount;
    }
}
