package com.infectedbytes.solarcolony;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Henrik on 23.12.2015.
 */
public interface ScreenState {
    void update(float delta);

    void render(SpriteBatch batch);

    void dispose();

    void resize(int width, int height);

    InputProcessor inputProcessor();
}
