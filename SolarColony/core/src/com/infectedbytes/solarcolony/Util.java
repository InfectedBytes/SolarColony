package com.infectedbytes.solarcolony;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Json;
import com.infectedbytes.solarcolony.enums.Resource;

import java.util.Random;

/**
 * Created by Henrik on 22.12.2015.
 */
public final class Util {
    private Util() {}

    public static final Random rnd = new Random();
    public static final int TILE_SIZE = 32;
    public static final int CONNECTOR_WIDTH = 24;
    public static final int CONNECTOR_THICKNESS = (TILE_SIZE - CONNECTOR_WIDTH) / 2;

    public static int CHARACTER_WIDTH = 24;
    public static int CHARACTER_HEIGHT = 32;

    public static final Json json = new Json() {{
        addClassTag("Connector", Connector.class);
        addClassTag("Storage", Storage.class);
        addClassTag("Resource", Resource.class);
        addClassTag("Room", Room.class);
        addClassTag("Quantity", Quantity.class);
    }};

    public static void floorCoords(Vector3 v) {
        v.x = MathUtils.floor(v.x / TILE_SIZE) * TILE_SIZE;
        v.y = MathUtils.floor(v.y / TILE_SIZE) * TILE_SIZE;
        v.z = MathUtils.floor(v.z / TILE_SIZE) * TILE_SIZE;
    }

    public static int floorCoord(float coord) { return MathUtils.floor(coord / TILE_SIZE) * TILE_SIZE; }

    public static void writeTexureRegion(Json json, TextureRegion region) {
        writeTexureRegion(json, null, region);
    }

    public static void writeTexureRegion(Json json, String name, TextureRegion region) {
        if(name != null) json.writeObjectStart(name);
        else json.writeObjectStart();
        json.writeValue("x", region.getRegionX());
        json.writeValue("y", region.getRegionY());
        json.writeValue("width", region.getRegionWidth());
        json.writeValue("height", region.getRegionHeight());
        json.writeObjectEnd();
    }

    public static float nextFloat() { return rnd.nextFloat(); }
    public static float nextFloat(float max) { return rnd.nextFloat() * max; }
    public static int nextInt(int max) { return rnd.nextInt(max); }
    public static boolean nextBool() {return rnd.nextBoolean(); }
}
