package com.infectedbytes.solarcolony.enums;

/**
 * Created by Henrik on 04.01.2016.
 */
public enum StorageType {
    Input(true, false), Output(false, true), Global(true, true);
    public final boolean isIn, isOut;

    StorageType(boolean in, boolean out) {
        isIn = in;
        isOut = out;
    }

    public static StorageType valueOf(int ordinal) {
        return values()[ordinal];
    }
}
