package com.infectedbytes.solarcolony.pathfinding;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.infectedbytes.solarcolony.Room;

/**
 * Created by Henrik on 26.12.2015.
 */
public class BasicGraphPath extends DefaultGraphPath<Connection<Room>> {
    public final Room start, end;

    public BasicGraphPath(Room start, Room end) {
        this.start = start;
        this.end = end;
    }

    public int getCost() {
        int cost = 0;
        for(Connection<Room> connection : this.nodes)
            cost += connection.getCost();
        return cost;
    }
}
