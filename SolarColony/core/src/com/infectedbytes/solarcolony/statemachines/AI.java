package com.infectedbytes.solarcolony.statemachines;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.infectedbytes.solarcolony.Citizen;
import com.infectedbytes.solarcolony.GameWorld;
import com.infectedbytes.solarcolony.Room;
import com.infectedbytes.solarcolony.Util;
import com.infectedbytes.solarcolony.enums.Direction;
import com.infectedbytes.solarcolony.enums.Message;
import com.infectedbytes.solarcolony.enums.Resource;
import com.infectedbytes.solarcolony.pathfinding.BasicGraphPath;

/**
 * Created by Henrik on 09.01.2016.
 */
public enum AI implements State<Citizen> {
    Idle() {
        @Override
        public boolean onMessage(Citizen entity, Telegram telegram) {
            if(!entity.isAbleToWork()) return false; // unable to work
            if(telegram.message == Message.MoveResources.ordinal()) {
                MoveJob job = (MoveJob)telegram.extraInfo;
                if(job.accepted) return false;
                job.accepted = true;
                entity.job = job;
                entity.stateMachine.changeState(GotoSource);
            }
            return false;
        }

        @Override
        public void update(Citizen entity) {
            entity.idletime += Gdx.graphics.getDeltaTime();
            boolean hungry = entity.isHungry();
            boolean thirsty = entity.isThirsty();
            if(entity.idletime > entity.hunger / 4) hungry = true; // so much idle, get some food!
            if(entity.idletime > entity.thirst / 4) thirsty = true; // so much idle, get some water!
            if(hungry && entity.getWorld().getGlobalAmount(Resource.RehydratableFood) > 0) {
                entity.job = new MoveJob(Resource.RehydratableFood, 1, null, entity);
                entity.job.optionalMultiplier = Util.nextInt(30);
                entity.stateMachine.changeState(GotoSource);
            } else if(thirsty && entity.getWorld().getGlobalAmount(Resource.Water) > 0) {
                entity.job = new MoveJob(Resource.Water, 1, null, entity);
                entity.job.optionalMultiplier = Util.nextInt(30);
                entity.stateMachine.changeState(GotoSource);
            }
        }

        @Override
        public void exit(Citizen entity) {
            entity.idletime = 0;
        }
    },

    GotoSource() {
        @Override
        public void update(Citizen entity) {
            if(!entity.hasJob()) {
                entity.stateMachine.revertToPreviousState();
                return;
            }
            MoveJob job = entity.job;
            GameWorld world = entity.getWorld();
            if(job.amount <= 0) {
                if(job.optionalMultiplier > 0) {
                    job.optionalMultiplier--;
                    if(world.getGlobalAmount(job.getResource()) >= job.initialAmount) {
                        job.amount = job.initialAmount;
                        return;
                    }
                }
                entity.stateMachine.popAndChangeState(DeliverGoods);
            } else if(job.from == null) {
                job.from = world.getClosestOutStorage(job.getResource(), entity.position);
            } else {
                if(GotoState.near(entity, job.from.getCenter())) {
                    job.grabResource(job.from);
                    job.from = null;
                } else gotoRoom(entity, job.from);
            }
        }
    },

    DeliverGoods() {
        @Override
        public void update(Citizen entity) {
            if(!entity.hasJob()) {
                entity.stateMachine.revertToPreviousState();
                return;
            }
            MoveJob job = entity.job;
            if(job.delivery.amount <= 0) {
                entity.job = null;
                entity.stateMachine.revertToPreviousState();
                return;
            }
            if(job.to == null) {
                job.to = entity.getWorld().getClosestInStorage(job.getResource(), entity.position);
            } else if(job.to instanceof Room) {
                Room to = (Room)job.to;
                if(GotoState.near(entity, to.getCenter())) {
                    job.deliverGoods();
                    job.to = null;
                } else gotoRoom(entity, to);
            } else {
                job.deliverGoods();
            }
        }
    };


    public void gotoRoom(Citizen entity, Room target) {
        GameWorld world = entity.getWorld();
        BasicGraphPath path = world.findPath(world.getRoom(entity.position), target);
        if(path != null) entity.stateMachine.changeState(new GotoState(path));
    }

    @Override
    public void enter(Citizen entity) {

    }

    @Override
    public void update(Citizen entity) {
        float rnd = Util.nextFloat();
        if(rnd < 0.0025f) {
            entity.stateMachine.changeState(new IdleWalk());
        } else if(rnd < 0.0075) {
            entity.heading = Direction.valueOf(Util.nextInt(4));
        }
    }

    @Override
    public void exit(Citizen entity) {

    }

    @Override
    public boolean onMessage(Citizen entity, Telegram telegram) {
        return false;
    }

    public static AI valueOf(int ordinal) { return values()[ordinal]; }
}
