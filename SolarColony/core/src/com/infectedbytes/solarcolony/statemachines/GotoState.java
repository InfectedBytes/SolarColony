package com.infectedbytes.solarcolony.statemachines;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.math.Vector2;
import com.infectedbytes.solarcolony.Citizen;
import com.infectedbytes.solarcolony.Connector;
import com.infectedbytes.solarcolony.Room;
import com.infectedbytes.solarcolony.Util;
import com.infectedbytes.solarcolony.enums.Direction;
import com.infectedbytes.solarcolony.enums.Message;
import com.infectedbytes.solarcolony.pathfinding.BasicGraphPath;

import java.util.Iterator;
import java.util.Random;

/**
 * Created by Henrik on 09.01.2016.
 */
public class GotoState extends StateAdapter<Citizen> {
    private static final float EPSILON = 4f;
    private BasicGraphPath path;
    private Iterator<Connection<Room>> iterator;
    private boolean lastStep;
    private Vector2 target;
    private Vector2 tmp = new Vector2();
    public static final float SPEED = 100;

    public GotoState(BasicGraphPath path) {
        this.path = path;
        this.iterator = path.iterator();
    }

    public static boolean near(Citizen entity, Vector2 target) {
        float dx = Math.abs(entity.position.x - target.x);
        float dy = Math.abs(entity.position.y - target.y);
        return dx < EPSILON && dy < EPSILON;
    }

    private float randomOffset() { return Util.nextFloat() * EPSILON - EPSILON / 2; }

    public boolean hasNext() { return iterator.hasNext(); }

    public Connector next() { return (Connector)iterator.next(); }

    @Override
    public void enter(Citizen entity) {
        entity.isWalking = true;
    }

    @Override
    public void exit(Citizen entity) {
        entity.isWalking = false;
    }

    @Override
    public void update(Citizen entity) {
        if(target == null || near(entity, target)) {
            if(hasNext()) target = next().getPosition();
            else if(!lastStep) {
                lastStep = true;
                target = path.end.getCenter();
            } else {
                entity.position.add(randomOffset(), randomOffset());
                entity.stateMachine.revertToPreviousState();
            }
        } else {
            float delta = SPEED * Gdx.graphics.getDeltaTime();
            tmp.set(target).sub(entity.position);
            entity.heading = Direction.valueOf(tmp.x, tmp.y);
            if(tmp.len() < delta) {
                entity.position.set(target);
            } else {
                tmp.nor().scl(delta);
                entity.position.add(tmp);
            }
        }
    }

    @Override
    public boolean onMessage(Citizen entity, Telegram telegram) {
        if(telegram.message == Message.RoomRemoved.ordinal()) {
            // path might be invalid now
            entity.stateMachine.revertToPreviousState();
        }
        return super.onMessage(entity, telegram);
    }
}
