package com.infectedbytes.solarcolony.statemachines;

import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.infectedbytes.solarcolony.GameEntity;
import com.infectedbytes.solarcolony.Quantity;
import com.infectedbytes.solarcolony.Room;
import com.infectedbytes.solarcolony.Storage;
import com.infectedbytes.solarcolony.enums.Resource;
import com.infectedbytes.solarcolony.enums.StorageType;

/**
 * Created by Henrik on 09.01.2016.
 */
public class MoveJob implements Json.Serializable {
    public int initialAmount;
    public int amount;
    public int optionalMultiplier;
    public Room from;
    public int fromID = -1;
    public GameEntity to; // has to be either a room (or null) or the entity itself
    public int toID = -1;
    public boolean accepted;
    public Quantity delivery;

    public Resource getResource() { return delivery.resource; }

    @Deprecated
    @SuppressWarnings("unused")
    public MoveJob() {}

    public MoveJob(Resource resource, int amount, Room from, GameEntity to) {
        if(from == null && to == null) throw new GdxRuntimeException("MoveJob from null to null");
        this.delivery = new Quantity(resource);
        this.initialAmount = amount;
        this.amount = amount;
        this.from = from;
        this.to = to;
    }

    @Override
    public void write(Json json) {
        json.writeValue("initialAmount", initialAmount);
        json.writeValue("amount", amount);
        json.writeValue("optionalMultiplier", optionalMultiplier);
        if(from != null) json.writeValue("from", from.getIndex());
        if(to instanceof Room) json.writeValue("to", ((Room)to).getIndex());
        json.writeValue("accepted", accepted);
        json.writeValue("delivery", delivery);

    }

    @Override
    public void read(Json json, JsonValue jsonData) {
        initialAmount = jsonData.getInt("initialAmount");
        amount = jsonData.getInt("amount");
        optionalMultiplier = jsonData.getInt("optionalMultiplier");
        if(jsonData.has("from")) fromID = jsonData.getInt("from");
        if(jsonData.has("to")) toID = jsonData.getInt("to");
        accepted = jsonData.getBoolean("accepted");
        delivery = json.readValue("delivery", Quantity.class, jsonData);
    }

    // Helper methods
    public void grabResource(Room room) {
        for(Storage storage : room.storages) {
            if(storage.getType().isOut) amount -= storage.get(delivery, amount);
        }
    }

    public void deliverGoods() {
        for(Storage storage : to.storages) {
            if(storage.getType() == StorageType.Input || storage.getType() == StorageType.Global)
                storage.add(delivery);
        }
    }
}
