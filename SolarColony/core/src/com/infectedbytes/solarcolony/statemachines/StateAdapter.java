package com.infectedbytes.solarcolony.statemachines;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import com.infectedbytes.solarcolony.Citizen;

/**
 * Created by Henrik on 09.01.2016.
 */
public abstract class StateAdapter<T> implements State<T> {
    @Override
    public void enter(T entity) { }

    @Override
    public void update(T entity) { }

    @Override
    public void exit(T entity) { }

    @Override
    public boolean onMessage(T entity, Telegram telegram) { return false; }
}
