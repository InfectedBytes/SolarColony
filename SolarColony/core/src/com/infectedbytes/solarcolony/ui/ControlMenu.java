package com.infectedbytes.solarcolony.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.infectedbytes.solarcolony.*;
import com.infectedbytes.solarcolony.useractions.DeleteAction;
import com.infectedbytes.solarcolony.useractions.PlaceCitizen;
import com.infectedbytes.solarcolony.useractions.PlaceRoom;

/**
 * Created by Henrik on 15.01.2016.
 */
public class ControlMenu extends Table {
    private static final int WIDTH = 180;
    private GameState gameState;

    public ControlMenu(final GameState gameState) {
        super(Resources.skin);
        this.gameState = gameState;
        setFillParent(true);
        left();

        button("Exit", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) { exitGame(); }
        }, null, "default").row();
        if(Main.godmodeUnlocked) {
            button("Godmode", new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) { gameState.toggleGodmode(); }
            }, null, "toggle").row();
        }
        button("Resource Screen", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) { gameState.toggleResourceScreen(); }
        }, null, "toggle").row();
        button("Place Character", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(gameState.godmode || gameState.getWorld().hasCloningFacility())
                    gameState.setUserAction(new PlaceCitizen(gameState));
                else gameState.popup("You need a Cloning Facility for this.");
            }
        }, "Creates a new human.\nNeeds a cloning facility.\nA new human costs " + Citizen.COST + " of Oxygen, Water and Biomass", "default").row();
        button("Save", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) { gameState.save(); }
        }, null, "default").row();
        button("Load", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) { load(); }
        }, null, "default").row();
        button("Destroy", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) { gameState.setUserAction(new DeleteAction(gameState)); }
        }, "Destroy a room.", "default").row();

        add(new Label("Rooms:", Resources.skin)).row();
        add(getRoomList()).width(WIDTH).maxHeight(200);
    }

    private void load() {
        Dialog dialog = new Dialog("Load", Resources.skin) {
            @Override
            protected void result(Object object) {
                if(object != null) gameState.load();
            }
        };
        dialog.text("Revert to last save?\nAll changes will be lost!");
        dialog.button("Revert", this);
        dialog.button("Cancel");
        dialog.show(getStage());
    }

    private void exitGame() {
        Dialog dialog = new Dialog("Exit", Resources.skin) {
            @Override
            protected void result(Object object) {
                if(object != null) gameState.exit();
            }
        };
        dialog.text("Do you really want to leave this session?\n" +
                "Unsaved changes will be lost!");
        dialog.button("Exit", this);
        dialog.button("Cancel");
        dialog.show(getStage());
    }

    @SuppressWarnings("unchecked")
    private ScrollPane getRoomList() {
        final Table list = new Table(Resources.skin);
        Array<Room> rooms = new Array<Room>();
        rooms.addAll(Util.json.fromJson(Array.class, Room.class, Gdx.files.internal("other.json")));
        rooms.addAll(Util.json.fromJson(Array.class, Room.class, Gdx.files.internal("storages.json")));
        rooms.addAll(Util.json.fromJson(Array.class, Room.class, Gdx.files.internal("generators.json")));
        for(final Room room : rooms) {
            final TextButton button = new TextButton(room.shortName, Resources.skin, "default");
            button.addListener(new TextTooltip(room.getTooltip(), Resources.skin));
            button.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) { gameState.setUserAction(new PlaceRoom(gameState, room)); }
            });
            list.add(button).width(160).height(24).row();
        }
        return new ScrollPane(list, Resources.skin);
    }

    private Cell<?> button(String text, ClickListener listener, String tooltip, String style) {
        TextButton btn = new TextButton(text, Resources.skin, style);
        btn.addListener(listener);
        if(tooltip != null) btn.addListener(new TextTooltip(tooltip, Resources.skin));
        return add(btn).width(WIDTH);
    }
}
