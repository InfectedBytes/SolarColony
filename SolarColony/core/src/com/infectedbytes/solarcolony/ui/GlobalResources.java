package com.infectedbytes.solarcolony.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.infectedbytes.solarcolony.GameState;
import com.infectedbytes.solarcolony.GameWorld;
import com.infectedbytes.solarcolony.Quantity;
import com.infectedbytes.solarcolony.Resources;
import com.infectedbytes.solarcolony.enums.Resource;

/**
 * Created by Henrik on 13.01.2016.
 */
public class GlobalResources extends Window {
    private GameState state;
    private Label[] amountLabels = new Label[Resource.values().length];

    public GlobalResources(GameState state) {
        super("Global Resources", Resources.skin);
        final float h = 16;
        this.state = state;
        this.setSize(260, 420);
        this.setPosition(Gdx.graphics.getWidth() - getWidth(), 0);

        Table table = new Table(Resources.skin);
        table.setFillParent(true);
        table.add(new Label(" Resource ", Resources.skin));
        table.add(new Label(" Amount ", Resources.skin));
        table.add(new Label(" Discard ", Resources.skin));
        table.row();
        for(final Resource resource : Resource.values()) {
            table.add(new Label(resource.shortName, Resources.skin)).height(h);
            Label label = new Label("0", Resources.skin);
            amountLabels[resource.ordinal()] = label;
            table.add(label).height(h);
            Table btns = new Table(Resources.skin);
            btns.add(discardButton(resource, 5)).height(20);
            btns.add(discardButton(resource, 25)).height(20);
            table.add(btns);
            table.row();
        }
        add(new ScrollPane(table, Resources.skin));
    }

    private TextButton discardButton(final Resource resource, final int amount) {
        TextButton btn = new TextButton("-" + amount, Resources.skin);
        btn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                discard(resource, amount);
            }
        });
        return btn;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        final GameWorld world = state.getWorld();
        for(final Resource resource : Resource.values()) {
            Label amount = amountLabels[resource.ordinal()];
            amount.setText(Integer.toString(world.getGlobalAmount(resource)));
        }
    }

    private void discard(Resource resource, int amount) {
        Quantity q = new Quantity(resource);
        state.getWorld().getGlobalResource(q, amount);
    }
}
