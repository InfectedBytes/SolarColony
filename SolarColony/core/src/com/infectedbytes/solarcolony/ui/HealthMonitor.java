package com.infectedbytes.solarcolony.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.infectedbytes.solarcolony.Citizen;
import com.infectedbytes.solarcolony.Resources;

/**
 * Created by Henrik on 13.01.2016.
 */
public class HealthMonitor extends Table {
    private ProgressBar health;
    private ProgressBar oxygen;
    private ProgressBar hunger;
    private ProgressBar thirst;
    private ShapeRenderer renderer;

    public HealthMonitor() {
        super(Resources.skin);
        float h = 18;
        renderer = new ShapeRenderer();
        add(new Label("Health: ", Resources.skin)).height(h);
        add(health = new ProgressBar()).row();
        add(new Label("Oxygen: ", Resources.skin)).height(h);
        add(oxygen = new ProgressBar(Citizen.LIMIT / 2)).row();
        add(new Label("Hunger: ", Resources.skin)).height(h);
        add(hunger = new ProgressBar()).row();
        add(new Label("Thirst: ", Resources.skin)).height(h);
        add(thirst = new ProgressBar()).row();
    }

    public void update(Citizen entity) {
        health.setValue(entity.health);
        oxygen.setValue(entity.breath);
        hunger.setValue(entity.hunger);
        thirst.setValue(entity.thirst);
    }

    private class ProgressBar extends Widget {
        private float value;
        private float threshold;

        public ProgressBar() { this(Citizen.THRESHOLD); }

        public ProgressBar(float threshold) { this.threshold = threshold; }

        @Override
        public float getPrefHeight() { return 10; }

        @Override
        public float getPrefWidth() { return 60; }

        public void setValue(float value) {
            this.value = MathUtils.clamp(value, 0, Citizen.LIMIT);
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            batch.end();
            if(value < threshold) renderer.setColor(Color.RED);
            else renderer.setColor(Color.GREEN);
            renderer.setProjectionMatrix(batch.getProjectionMatrix());
            renderer.begin(ShapeRenderer.ShapeType.Filled);
            renderer.rect(getX(), getY(), value * getWidth() / Citizen.LIMIT, getHeight());
            renderer.end();
            renderer.setColor(Color.WHITE);
            renderer.begin(ShapeRenderer.ShapeType.Line);
            renderer.rect(getX(), getY(), getWidth(), getHeight());
            renderer.end();
            batch.begin();
        }
    }
}
