package com.infectedbytes.solarcolony.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.infectedbytes.solarcolony.GameState;
import com.infectedbytes.solarcolony.Main;
import com.infectedbytes.solarcolony.Resources;

/**
 * Created by Henrik on 14.01.2016.
 */
public class SavegameDialog extends Dialog {
    private FileHandle template;
    private FileHandle directory;
    private TextField input;
    private Main main;
    private static final int MaxLength = 20;

    public SavegameDialog(Main main, String template) {
        super("New Game", Resources.skin);
        this.main = main;
        this.template = Gdx.files.internal(template);
        this.directory = Gdx.files.local("savegames");
        if(!directory.exists()) directory.mkdirs();

        getContentTable().add(new Label("Enter a new name for your savegame: ", Resources.skin)).row();
        input = new TextField(newName(), Resources.skin);
        getContentTable().add(input).row();

        button("OK", this); // just add any object
        button("Cancel");
    }

    @Override
    protected void result(Object object) {
        if(object != null) { // OK button
            if(input.getText().length() > MaxLength) {
                Dialog dialog = new Dialog("Error", Resources.skin);
                dialog.text("Name too long!\n" +
                        "Please enter at most " + MaxLength + " characters.");
                dialog.button("OK");
                dialog.show(getStage());
                cancel();
            } else if(!valid(input.getText())) {
                Dialog dialog = new Dialog("Error", Resources.skin);
                dialog.text("Invalid name!\n" +
                        "Please use only the following characters:\n" +
                        "A-Z 0-9 . - _");
                dialog.button("OK");
                dialog.show(getStage());
                cancel();
            } else if(fileExist(input.getText())) {
                Dialog dialog = new Dialog("Error", Resources.skin);
                dialog.text("The file does already exist!");
                dialog.button("OK");
                dialog.show(getStage());
                cancel();
            } else {
                template.copyTo(directory.child(input.getText()));
                main.push(new GameState(main, input.getText()));
            }
        }
    }

    @Override
    public void hide() {
        removeCaptureListener(ignoreTouchDown);
        remove();
    }

    private String newName() {
        int id = 0;
        FileHandle file;
        while((file = directory.child("savegame" + id + ".sav")).exists()) id++;
        return file.name();
    }

    private boolean valid(String name) { return name.length() <= MaxLength && name.matches("^[\\w\\d\\.\\-_]+$"); }

    private boolean fileExist(String name) {
        return directory.child(name).exists();
    }
}
