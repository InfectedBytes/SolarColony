package com.infectedbytes.solarcolony.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.infectedbytes.solarcolony.GameState;
import com.infectedbytes.solarcolony.Resources;
import com.infectedbytes.solarcolony.enums.Resource;
import com.infectedbytes.solarcolony.useractions.InfoAction;

/**
 * Created by Henrik on 04.01.2016.
 */
public class StatusBar extends Table {
    private GameState gameState;
    private TextButton pauseButton;

    public boolean isPaused() { return pauseButton.isChecked(); }

    public StatusBar(final GameState gameState) {
        this.setFillParent(true);
        this.top();
        this.gameState = gameState;
        add(pauseButton = new TextButton("Pause", Resources.skin, "toggle"));
        {
            TextButton info = new TextButton("Info", Resources.skin);
            info.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    gameState.setUserAction(new InfoAction(gameState));
                }
            });
            add(info);
        }
        addResourceLabel(Resource.Energy);
        addResourceLabel(Resource.Oxygen);
        addResourceLabel(Resource.Water);
        addResourceLabel(Resource.RehydratableFood);
    }

    private void addResourceLabel(Resource resource) {
        add(new Label(resource.shortName + ": ", Resources.skin));
        add(new ResourceLabel(resource)).width(60);
    }

    private class ResourceLabel extends Label {
        private Resource resource;

        public ResourceLabel(Resource resource) {
            super("0", Resources.skin);
            this.resource = resource;
        }

        @Override
        public void act(float delta) {
            super.act(delta);
            this.setText(Integer.toString(gameState.getWorld().getGlobalAmount(resource)));
        }
    }
}
