package com.infectedbytes.solarcolony.useractions;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.infectedbytes.solarcolony.GameState;
import com.infectedbytes.solarcolony.GameWorld;

/**
 * Created by Henrik on 24.12.2015.
 */
public abstract class UserAction {
    protected GameState gameState;
    protected boolean middleDown;
    private int dragX, dragY;

    protected GameWorld getWorld() { return gameState.getWorld(); }

    public UserAction(GameState state) {
        this.gameState = state;
    }

    public void update(float delta) { }

    public abstract void render(SpriteBatch batch);

    public void mouseMoved(int worldX, int worldY) { }

    public void mouseDragged(int worldX, int worldY) {
        if(middleDown) {
            gameState.getCamera().translate(dragX - worldX, dragY - worldY);
        }
    }

    public void mouseDown(int worldX, int worldY, int button) {
        if(button == 1) gameState.setUserAction(null);
        if(button == 2) {
            middleDown = true;
            dragX = worldX;
            dragY = worldY;
        }
    }

    public void mouseUp(int worldX, int worldY, int button) {
        if(button == 2) middleDown = false;
    }
}
